document.addEventListener("DOMContentLoaded", function() {
    const yearSpan = document.querySelector('#year');
    yearSpan.innerText = new Date().getFullYear();
  });